'use strict';

class RuntimeError extends BaseError{
    constructor(){
        super(500, "Internal Server Error");
    }
}