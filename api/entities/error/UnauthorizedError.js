'use strict';

class UnauthorizedError extends BaseError{
    constructor(){
        super(401, "Unauthorized");
    }
}