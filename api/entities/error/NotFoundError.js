'use strict';

class NotFoundError extends BaseError{
    constructor(){
        super(500, "Not Found");
    }
}