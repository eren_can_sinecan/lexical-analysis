'use strict';

class InvalidInputError extends BaseError{
    constructor(){
        super(400, "Invalid Input");
    }
}