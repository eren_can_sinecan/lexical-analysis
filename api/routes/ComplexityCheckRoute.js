'use strict';
const ResponseSender = require("./util/ResponseSender");
const ComplexityCheckValidator = require("../validators/ComplexityCheckValidator");
const ComplexityCheckService = require("../services/ComplexityCheckService");
const complexityCheckService = new ComplexityCheckService();

const calculateDensity = async (req, res) => {
    try{
        let {text, mode} = req.swagger.params;
        let isVerbose = (!!mode) && (mode === 'verbose');
        //TODO handle verbose
        //TODO lint
        ComplexityCheckValidator.validateTextLength(text.value);
        ComplexityCheckValidator.validateWordCount(text.value);
        let responseBody = await complexityCheckService.calculateDensity(text.value, isVerbose);
        ResponseSender.sendResponse(res, responseBody);
    }catch(e){
        ResponseSender.sendResponse(res, e);
    }
};