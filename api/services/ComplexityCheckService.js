'use strict';
const WordModel = require("../entities/model/WordModel");
const RuntimeError = require("../entities/error/RuntimeError");

class ComplexityCheckService {

    /**
     * * I thought of two approaches. One is loading the list of nonlexical words at
     * the application start and caching them, updating only when there's a new word added.
     * However, I'll take the approach of querying each word individually in
     * our nonlexical word database.
     * @param text
     * @param isVerbose
     * @returns {Promise<void>}
     */
    async calculateDensity(text, isVerbose){
        try{
            let lexicalDensity = {};
            lexicalDensity.overall_ld = await this.getLexicalDensity(text);

            if(isVerbose){
                lexicalDensity.sentence_ld = text.split(".").map((sentence) => {
                    let sentenceLd = await this.getLexicalDensity(sentence);
                    return sentenceLd;
                })
            }

        }
        catch (e) {
            console.error(e);
            throw new RuntimeError();
        }
    }

    async getLexicalDensity(text){
        //TODO remove punctuation
        let textAsWords = text.split(" ");
        const totalWordCount = textAsWords.length;
        let occurences = this.getOccurences(textAsWords);
        let lexicalCount = 0;
        Object.keys(occurences).map(async (word) => {
            let nonLexical = await this.isNonLexical(word);
            if(nonLexical){
                lexicalCount += occurences[word];
            }
        })
    }

    async isNonLexical(word){
        return new Promise((resolve, reject) => {
            resolve(true);
        })
    }

    /**
     * Counts number of occurences of each word.
     * @param text
     * @private
     */
    getOccurences (textAsWords){
        return textAsWords.reduce((accumulator, currentValue) => {
            !accumulator[currentValue] ? accumulator[currentValue] = 1 : accumulator[currentValue]++;
            return accumulator;
        }, {})
    }
}
module.exports = ComplexityCheckService;